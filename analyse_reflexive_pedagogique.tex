\documentclass[french,a4paper,titlepage,oneside,final,12pt]{article}

\newcommand{\NOM}{\bsc{David}}%
\newcommand{\PRENOM}{Grégory}%

\usepackage[margin=2.5cm]{geometry}
\usepackage[french]{babel}
\usepackage[utf8]{inputenc}
\usepackage{graphicx}
\usepackage[pdftex]{hyperref}
\usepackage[usenames]{color}
\definecolor{DarkGreen}{rgb}{0,0.75,0}
\definecolor{DarkRed}{rgb}{0.5,0,0}
\definecolor{DarkBlue}{rgb}{0,0,0.75}
\definecolor{DarkGrey}{rgb}{0.2,0.2,0.2}

\usepackage{fancyhdr}%
\pagestyle{fancy}%
\fancyhead[L]{Analyse réflexive pédagogique}%
\fancyhead[R]{\PRENOM{} \NOM{}}%

\title{Analyse réflexive sur ma pratique pédagogique}%
\author{\PRENOM{} \NOM{}\\Lycée André \bsc{Malraux}\\3 Rue de
  Beausoleil\\72700 Allonnes}%
\date{inspection du 19 décembre 2018\\par Didier \textsc{Gouill}\\IA
  IPR Économie \& Gestion\\Académie de Nantes}%

\begin{document}
\begin{titlepage}
  \maketitle
\end{titlepage}

\begin{abstract}
  Enseignant à l'Éducation Nationale depuis 2005, j'ai pu mettre en
  \oe uvre dès le début de ma carrière des méthodologies et une
  pédagogie issues de mes expériences d'animation et de formation,
  ainsi que de ma formation à l'IUFM qui ont semblé être
  satisfaisantes.

  J'ai pu identifier des méthodes pertinentes, et d'autres beaucoup
  moins. Ne pensant pas qu'il soit suffisant de se contenter des
  éléments vus durant ma formation à l'IUFM, j'ai cherché à imaginer
  de nouveaux moyens, de nouvelles méthodes, permettant d'obtenir une
  meilleure qualité d'enseignement, du moins une meilleure pratique.

  Sans être certain de la pertinence de ces méthodes, j'identifie
  cependant des avantages à ces expérimentations : la réactivité, la
  disponibilité, la précision.

  Étant loin de me fixer sur une pédagogie en particulier, j'opte pour
  l'adaptation afin de m'octroyer un recul suffisant, offert par
  l'expérience de plusieurs cohortes d'apprenants, sur des sujets et
  champs disciplinaires différents.
\end{abstract}

Depuis le début de ma carrière, j'enseigne presque exclusivement en
section de technicien supérieur en informatique. Mes interventions en
secondes IGC, puis terminale S spécialité ISN, ne me permettent pas
d'en tirer autre chose qu'une expérience insuffisante à l'analyse. Je
porterais donc exclusivement ma réflexion sur mes interventions en
section de technicien supérieur en informatique.

\section*{Expérience concrète}
\label{sec:ExperienceConcrete}
Dans les quatre premières années de ma carrière (2005-2009), j'ai mis
en \oe uvre une pédagogie conventionnelle à celle que j'avais pu
cotoyer, mais sans y être formé, durant ma formation à l'IUFM. Ainsi,
je construisais méticuleusement mes séances d'intervention en me
basant sur le modèle suivant :
\begin{enumerate}
  \item identification des objectifs pédagogiques opérationnels,
  \item élaboration de séquence courte de transmission du savoir,
  \item mise en \oe uvre d'exercices d'applications pré-conçus
  prenant en compte les difficultés \emph{a priori},
  \item évaluation formative ou sommative.
\end{enumerate}

Dans ce contexte pédagogique d'enseignement transmissif, mon objectif
principal était d'avoir confiance en moi et de transmettre ce que
j'identifiais comme étant le savoir.

Ce contexte était très éprouvant, car étant en début de carrière,
beaucoup de choses me semblaient difficiles à surmonter -- comme
l'élaboration documentaire, la construction de sujet de TP
pertinents. Je n'arrivais pas à en voir une claire finalité
opérationnelle et finissais par être emprisonné par la forme immuable
de mes documents et TP.


\section*{Observation réflexive}
\label{sec:ObservationReflexive}

\subsection*{Début de carrière}
Lors des séances, ayant travaillé avec vigueur à l'élaboration d'un
scénario pédagogique, je m'organisais au mieux pour être en adéquation
avec ma planification. Si j'avais envisagé de faire un exercice après
avoir traité une partie du sujet, alors cet exercice se devait d'être
réalisé. Cependant, cette pugnacité à rester concentré à ma propre
construction intellectuelle m'a clairement empêché d'être attentif à
ce qui est le plus important : l'apprenant et sa psychologie.

De formation plutôt universitaire, je n'avais pas de mal à concevoir
mes interventions comme des séances de transmission didactique. Le
sujet principal étant alors la matière, le champ disciplinaire, le
savoir savant.

De toute évidence, ce qui était fonctionnel dans cette construction
était l'aisance que j'avais à faire mes interventions ; me centrant
sur le savoir, je pouvais m'échapper (inconsciemment et
involontairement) de la nature même de ma mission : la considération
réelle de la difficulté de l'apprenant.

Pour autant, les étudiants ne semblaient pas se désintéresser de la
matière -- GEOSI à l'époque de 2009 -- pour laquelle j'affectionnais
particulièrement les contraintes organisationnelles liées aux
problématiques de sécurité. Leurs remarques étaient plutôt
encouragentes, et me confortaient dans l'idée de fournir du contenu
exigent.

Cependant, le plus gros écueil était d'être négligent quant aux
difficultés particulières d'apprentissage.

C'est alors qu'il est devenu indispensable d'envisager autre chose,
une autre façon d'apporter la connaissance. Ce qui était
d'ailleurs mal dit, mal formulé. Car finalement, il ne s'agissait plus
d'\emph{apporter la connaissance}, mais plutôt de \emph{trouver un
  moyen de la susciter}.

La problématique devenait alors plus intéressante, moins dirigée vers
une mise en \oe uvre de méthodologies conçues par
d'autres. \emph{Il me fallait élaborer une pédagogie personnelle} dont
l'objectif principal serait d'optimiser le lien entre le savoir et
l'apprenant.

\subsection*{Depuis la rénovation en BTS SIO}

La rénovation du BTS Informatique de Gestion en BTS Services
Informatiques aux Organisations a généré une contrainte supplémentaire
: le niveau d'entrée des apprenants est devenu plus faible tandis que
le niveau de sortie escompté est devenu plus haut.

Dès lors, les questions liées à la transmission des savoirs et
savoirs-faire ont pris une dimension plus vertigineuse. En effet,
l'équation initiale de l'enseignement est déjà difficile à résoudre,
et y rajouter cette nouvelle composante était \emph{a priori} une
gageure.

\paragraph{Susciter la connaissance est-il toujours possible ?}

J'envisageais alors une structuration selon :
\begin{itemize}
  \item l'\textbf{observation} afin de \textbf{proposer} un outil de
  remédiation \textbf{approprié} pour favoriser l'apprentissage,
  \item la \textbf{reconstruction} du savoir à partir du besoin,
  \item le fait de faire acquérir des compétences et des connaissances
  par \textbf{élaboration collective ou individuelle} en faisant
  \textbf{coopérer} les différents membres du groupe classe.
\end{itemize}

\section*{Expérimentation active}
\label{sec:ExperimentationActive}
Mon intention était donc de mettre en \oe uvre une approche de nature
constructiviste\footnote{\textit{L'approche constructiviste met en
    avant l'activité et la capacité inhérentes à chaque sujet, ce qui
    lui permet d'appréhender la réalité qui l'entoure.}
  \cite{piaget1936naissance}}, mais sans en appliquer le modèle de
façon dogmatique.

Par conséquent, j'ai identifié le matériel de remédiation dont je
disposait : théorie, exercices, travaux pratiques, études de cas. J'ai
cherché à identifier plus nettement mes limites cognitives, tout en
les mettant en relief avec le référentiel de certification et mes
expériences artistiques.

Ainsi, je me définissais un bagage conséquent qui allait me permettre
d'être \textbf{réactif}. En effet, mon expérimentation me fait
désormais envisager la séance non plus comme un film à dérouler, mais
comme un \textbf{apprentissage à construire avec les apprenants}.

Dans les faits, cela ressemble à une élaboration d'échanges pendant
lesquelles je guide le sujet vers l'objectif cognitif le plus proche
du lieu où nous sommes, en prenant soin de délimiter le besoin
initial à une situation souvent pragmatique ou concrète.

Je tente alors d'avoir une visibilité du référentiel en tête
et construit le savoir avec les apprenants, en fonction de ce qu'ils
sont, et comme ils sont au moment de mon intervention.

C'est alors un jeu extrêment subtil entre la vie qui se déroule dans
l'intimité de la classe et ma mission d'enseignant qui devient
hyper-(ré)active, rebondissant de difficultés en remédiations, de
sujets en notions, de savoirs en savoirs-faire.

Partant de l'idée initiale visant à fournir un terreau expérimental
pour l'apprenant, je réalise que je me rapproche d'une forme de
mélange entre pédagogies Montessori, Freinet, Constructiviste et
Coopérative.


\section*{Évaluation de la démarche}
\label{sec:evaluation.continue}
Cette méthodologie, ou plus réellement cette manière d'appréhender le
métier d'enseignant, me pousse à évaluer constamment ma propre
pratique et de qualifier la réussite de chaque séance. L'inconvénient
c'est que cela sollicite particulièrement mon perfectionnisme. Pour
autant, ceci est intellectuellement excitant et me semble permettre de
fournir aux apprenants une vision du savoir moins abstraite.

Par ailleurs, dans cette démarche, j'estime beaucoup plus mon rôle et
la position que j'incarne dans la relation avec chacune des cohortes.

\bibliography{analyse_reflexive_pedagogique}
\bibliographystyle{alphaurl} \nocite{*}

\end{document}
